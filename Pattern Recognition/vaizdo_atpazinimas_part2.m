close all
clear all
clc

%% read the image with hand-written characters
pavadinimas = 'train_data.png';
pozymiai_tinklo_mokymui = pozymiai_raidems_atpazinti(pavadinimas, 5);
actual_string = "ABCDEFGHIJK";

%% Development of character recognizer

% take the features from cell-type variable and save into a matrix-type variable
P = cell2mat(pozymiai_tinklo_mokymui);

% create the matrices of correct answers for each line (number of matrices = number of symbol lines)
T = [eye(11), eye(11), eye(11), eye(11), eye(11)];

% create an RBF network for classification with 13 neurons, and sigma = 1
intvl = [min(P')' max(P')'];
tinklasH = newff(intvl,[13 13],{'tansig' 'purelin'}, 'traingdx', 'learngdm', 'mse');
tinklasL = newff(intvl,[13 5],{'tansig' 'purelin'}, 'traingdx', 'learngdm', 'mse');


%% Tinklo patikra | Test of the network (recognizer)

% estimate output of the network for unknown symbols (row, that were not used during training)
P2 = P(:,12:22);
Y2H = sim(tinklasH, P2);
Y2L = sim(tinklasL, P2);

% find which neural network output gives maximum value
[a2H, b2H] = max(Y2H);
[a2L, b2L] = max(Y2L);


%% Visualize result

% calculate the total number of symbols in the row
raidziu_sk = size(P2,2);

% we will save the result in variable 'atsakymas'
atsakymasH = [];
atsakymasL = [];

for k = 1:raidziu_sk
    switch b2H(k)
        case 1
            % the symbol here should be the same as written first symbol in your image
            atsakymasH = [atsakymasH, 'A'];
        case 2
            atsakymasH = [atsakymasH, 'B'];
        case 3
            atsakymasH = [atsakymasH, 'C'];
        case 4
            atsakymasH = [atsakymasH, 'D'];
        case 5
            atsakymasH = [atsakymasH, 'E'];
        case 6
            atsakymasH = [atsakymasH, 'F'];
        case 7
            atsakymasH = [atsakymasH, 'G'];
        case 8
            atsakymasH = [atsakymasH, 'H'];
        case 9
            atsakymasH = [atsakymasH, 'I'];
        case 10
            atsakymasH = [atsakymasH, 'J'];
        case 11
            atsakymasH = [atsakymasH, 'K'];
    end
end

for k = 1:raidziu_sk
    switch b2L(k)
        case 1
            % the symbol here should be the same as written first symbol in your image
            atsakymasL = [atsakymasL, 'A'];
        case 2
            atsakymasL = [atsakymasL, 'B'];
        case 3
            atsakymasL = [atsakymasL, 'C'];
        case 4
            atsakymasL = [atsakymasL, 'D'];
        case 5
            atsakymasL = [atsakymasL, 'E'];
        case 6
            atsakymasL = [atsakymasL, 'F'];
        case 7
            atsakymasL = [atsakymasL, 'G'];
        case 8
            atsakymasL = [atsakymasL, 'H'];
        case 9
            atsakymasL = [atsakymasL, 'I'];
        case 10
            atsakymasL = [atsakymasL, 'J'];
        case 11
            atsakymasL = [atsakymasL, 'K'];
    end
end

% show the result in command window
figure(1)
subplot(3,1,1), text(0.1,0.5,actual_string,'FontSize',38), title("Training Actual Characters"), axis off
subplot(3,1,2), text(0.1,0.5,atsakymasH,'FontSize',38), title("RBFN 13 13 Nodes Result"), axis off
subplot(3,1,3), text(0.1,0.5,atsakymasL,'FontSize',38), title("RBFN 13 5 Nodes Result"), axis off


%% Extract features of the test image
pavadinimas = 'test_kada.png';
pozymiai_patikrai = pozymiai_raidems_atpazinti(pavadinimas, 1);
actual_string = "ABHIECKFDG";


%% Perform letter/symbol recognition

% features from cell-variable are stored to matrix-variable
P2 = cell2mat(pozymiai_patikrai);

% estimating neuran network output for newly estimated features
Y2H = sim(tinklasH, P2);
Y2L = sim(tinklasL, P2);

% searching which output gives maximum value
[a2H, b2H] = max(Y2H);
[a2L, b2L] = max(Y2L);


%% Rezultato atvaizdavimas | Visualization of result

% calculating number of symbols - number of columns
raidziu_sk = size(P2,2);
% rezultatà saugosime kintamajame 'atsakymas'
atsakymasH = [];
atsakymasL = [];

for k = 1:raidziu_sk
    switch b2H(k)
        case 1
            atsakymasH = [atsakymasH, 'A'];
        case 2
            atsakymasH = [atsakymasH, 'B'];
        case 3
            atsakymasH = [atsakymasH, 'C'];
        case 4
            atsakymasH = [atsakymasH, 'D'];
        case 5
            atsakymasH = [atsakymasH, 'E'];
        case 6
            atsakymasH = [atsakymasH, 'F'];
        case 7
            atsakymasH = [atsakymasH, 'G'];
        case 8
            atsakymasH = [atsakymasH, 'H'];
        case 9
            atsakymasH = [atsakymasH, 'I'];
        case 10
            atsakymasH = [atsakymasH, 'J'];
        case 11
            atsakymasH = [atsakymasH, 'K'];
    end
end

for k = 1:raidziu_sk
    switch b2L(k)
        case 1
            atsakymasL = [atsakymasL, 'A'];
        case 2
            atsakymasL = [atsakymasL, 'B'];
        case 3
            atsakymasL = [atsakymasL, 'C'];
        case 4
            atsakymasL = [atsakymasL, 'D'];
        case 5
            atsakymasL = [atsakymasL, 'E'];
        case 6
            atsakymasL = [atsakymasL, 'F'];
        case 7
            atsakymasL = [atsakymasL, 'G'];
        case 8
            atsakymasL = [atsakymasL, 'H'];
        case 9
            atsakymasL = [atsakymasL, 'I'];
        case 10
            atsakymasL = [atsakymasL, 'J'];
        case 11
            atsakymasL = [atsakymasL, 'K'];
    end
end


% pateikime rezultatà komandiniame lange
figure(2),
subplot(3,1,1), text(0.1,0.5,actual_string,'FontSize',38), title("Test Actual Characters"), axis off
subplot(3,1,2), text(0.1,0.5,atsakymasH,'FontSize',38), title("RBFN 13 13 Nodes Result"), axis off
subplot(3,1,3), text(0.1,0.5,atsakymasL,'FontSize',38), title("RBFN 13 5 Nodes Result"), axis off
