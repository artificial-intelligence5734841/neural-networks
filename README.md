# Multi-Layer Perceptron and Radial Basis Function Neural Network in MATLAB

## Overview

This repository contains MATLAB scripts for implementing neural networks: a Multi-Layer Perceptron (MLP) with Back Propagation and a Radial Basis Function Neural Network (RBFN). Additionally, it includes an application of RBFN for pattern recognition. These scripts are designed to demonstrate basic neural network principles and techniques for function approximation and pattern recognition.

## Files

### 1. `MLP.m`
This script demonstrates the use of a Multi-Layer Perceptron (MLP) with Back Propagation (BP) for two tasks:
- **Task I**: Approximation of single input data.
- **Task II**: Surface approximation using two input variables.

#### Key Features
- **Task I**: 
  - Constructs data using a sine function.
  - Configures the MLP with one hidden layer.
  - Uses a sigmoidal activation function.
  - Runs the MLP BP algorithm to approximate the function.

- **Task II**: 
  - Constructs random input data with two variables.
  - Configures the MLP with two hidden layers.
  - Uses a hyperbolic tangent activation function.
  - Runs the MLP BP algorithm to approximate a surface.

#### Functions
- `random_function`: Generates random polynomial functions for data generation.
- `activation_function`: Defines the sigmoidal and hyperbolic tangent functions and their derivatives.
- `MLP_BP`: Implements the MLP with Back Propagation algorithm, including forward feed and backpropagation steps.

### 2. `RBFN.m`
This script demonstrates the use of a Radial Basis Function Neural Network (RBFN) for function approximation.

#### Key Features
- Constructs data using a sine function.
- Defines radial basis functions (RBF) with specific centers and radii.
- Trains the RBFN using a simple learning algorithm.
- Approximates the function and plots the results.

#### Functions
- `F`: Defines the radial basis function used in the RBFN.

### 3. Pattern Recognition Utilizing RBFN Application
It applies the Radial Basis Function Neural Network (RBFN) for pattern recognition using the built-in MATLAB function `newrb()`. The application is trained to recognize the letters "A" through "K" from handwritten inputs.

#### Key Features
- Uses `newrb()` to create and train the RBFN.
- Takes handwritten letter inputs.
- Recognizes and outputs the closest matching letter from the trained set.

#### Usage
1. **Data Preparation**:
   - Prepare a dataset of handwritten letters "A" to "K".
   - Each letter should be represented in a format suitable for training the RBFN.

2. **Training**:
   - Use the `newrb()` function to train the RBFN on the prepared dataset.

3. **Prediction**:
   - Input a new handwritten letter.
   - The script will output the closest matching letter from the trained set.

## Example Outputs

### MLP
The `MLP.m` script will display the results of the function and surface approximation tasks, including the number of epochs and total error for each task. If plotting is enabled, it will generate graphs showing the evolution of the approximation over epochs.

### RBFN
The `RBFN.m` script will generate a plot comparing the original data and the approximated function using the trained RBFN.

### Pattern Recognition with RBFN
The Pattern Recognition script will output the recognized letter when a new handwritten letter is input. It demonstrates the pattern recognition capabilities of the RBFN.