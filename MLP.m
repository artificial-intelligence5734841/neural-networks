%% Clear Variables
clear
clc

%% I - Approximation of Data using MLP BP (1 1 1) (NbreInputs NbreHiddenLayers NbreOutputs)
% Data Construction
x = (0.1: 1/22 :1)';
data = ((1 + 0.6 * sin (2 * pi * x / 0.7)) + 0.3 * sin (2 * pi * x) / 2);


% Number of Nodes of Each Hidden Layer
n_nodes_hidLayers = 4;% One value means one Hidden Layer
 
% Type of Activation Function (sigmoidal | hyperbolic_tangent)
activation_type = 'sigmoidal';

% Learning Rate
eta = 0.3;

% Maximum Epochs & Error
cycles = 500;
max_error = 0;

% Evolution plot of MLP over epochs:
% Options:
%       0: Plot
%       Any other number: Discard plotting
plotting = 0;

% MLP BP Solution
[~, ~, ~, epochs, error] = MLP_BP(n_nodes_hidLayers, x, data, activation_type, eta, cycles, max_error, plotting);
fprintf('Task I Results:\nNumber of epochs = %d\nTotal Error = %e\n', epochs, error);


%% II - Surface Appriximation (2 L 1) (Two inputs; L hidden layer; One output)
% Data construction (2 inputs variables with 5 data each -> Total Inputs combination = (5*2)^2 = 100)
[n, m] = deal(5, 2);
points = rand(n, m);
grid_pt = cell(1, m);
[grid_pt{:}] = ndgrid(points);
x = zeros((m*n)^m, m);
for i = 1:m
    x(:,i) = grid_pt{i}(:);
end
[data, deg, coeff] = random_function(x);


% List of Number of Nodes of Each Hidden Layer
n_nodes_hidLayers = [4 4];

% Type of Activation Function (sigmoidal | hyperbolic_tangent)
activation_type = 'hyperbolic_tangent';

% Learning Rate
eta = 0.2;

% Maximum Epochs & Error
cycles = 500;
max_error = 0;

% Evolution plot
plotting = 0;

% MLP BP Solution
%{
The performance of the MLP BP Algorithm depends on its structure. Since random_function(x) randomly selects the degrees
and the coefficients, the number of hidden layers and their nodes is not constant. 
In this example, the MLP BP has 2 hidden layers with 3 nodes each for reduced complexity.
%}
[weights, bias, y_hat, epochs, error] = MLP_BP(n_nodes_hidLayers, x, data, activation_type, eta, cycles, max_error, plotting);

fprintf('\nTask II Results:\nNumber of epochs = %d\nTotal Error = %e\n', epochs, error);


%% Functions Definition
% Random Function (m inputs, 1 output)
function [y, deg, coeff] = random_function(x)
    m = length(x(1,:));

    deg = randi([2 5], 1,m);
    coeff = (rand(1,m) .* randi([-10 10], 1,m))';
    y = (x .^ deg) * coeff;
end

% Activation Function
function r = activation_function(v, type, derivate_order)
    if(strcmp(type, 'hyperbolic_tangent'))
        if(derivate_order==0)
            r = tanh(v);
        end
        if(derivate_order==1)
            r = 1 - tanh(v).^2;
        end
    end

    if(strcmp(type, 'sigmoidal'))
        if(derivate_order==0)
            r = 1./(1+exp(-v));
        end

        if(derivate_order==1)
            r = exp(-v)./((1+exp(-v)).^2);
        end
    end
end

% MLP Back Propagation Algorithm
function [weights, bias, y_hat, epochs, error] = MLP_BP(n_nodes_hidLayers, x, data, activation_type, eta, iter, max_error, plotting)
    % 0. Initiating Variables
    % Utility variables
    [n, m] = size(x);
    
    n_ouputs = length(data(1));
    lays_nodes = [n_nodes_hidLayers n_ouputs];
    all_nodes = [m lays_nodes];
    dw = [length(lays_nodes) max(lays_nodes)];

    % Perceptron Variables
    w = cell(1, dw(1));
    b = zeros(dw);
    v = zeros(dw);
    y = zeros(n, n_ouputs);

    d = zeros(dw);

    % 1. Initiating Weights & biases
    for i = 1:dw(1)
        w{i} = rand(all_nodes(i:i+1));
        b(i, 1:lays_nodes(i)) = rand(1, lays_nodes(i));
    end


    % 2. MLP Algorithm - Back Propagation
    % 2.1. Forward Feed
    cyc = 0;
    indx = iter/10;

    while cyc ~= iter
        for row = 1:n
            % Computing weighted sums for first Hidden Layer
            c = lays_nodes(1);
            v(1, 1:c) = x(row,:) * w{1} + b(1, 1:c);
            v(1, 1:c) = activation_function(v(1, 1:c), activation_type, 0);
            
            for i = 2:dw(1)
                p = lays_nodes(i-1);
                c = lays_nodes(i);
    
                % Computing the weighted sums
                v(i, 1:c) = v(i-1, 1:p) * w{i} + b(i, 1:c);
    
                % Applying linear activation for the output nodes
                if(i == dw(1))
                    continue;
                end
                % Applying the activation function
                v(i, 1:c) = activation_function(v(i, 1:c), activation_type, 0);
            end
            y(row,:) = v(end, 1:c);


            % 2.2. Training Algorithm - Back Propagation
            e = data(row,:) - y(row,:);

            c = lays_nodes(end);
            d(end, 1:lays_nodes(end)) = e .* activation_function(v(end,1:c), activation_type, 1);
            w{end} = w{end} + eta * d(end,1:c) .* y(row,:);
            b(end,1:c) = b(end,1:c) + eta * d(end,1:c);
    
            for i = 1:dw(1)-1
                p = lays_nodes(end-i+1);
                c = lays_nodes(end-i);
                d(end-i,1:c) = activation_function(v(end-i,1:c), activation_type, 1) .* (d(end-i+1,1:p) * w{end-i+1}');
                b(end-i,1:c) = b(end-i,1:c) + eta * d(end-i,1:c);
                
                if(i == dw(1)-1)
                    w{end-i} = w{end-i} + eta * d(end-i,1:c) .* x(row,:)';
                else
                    w{end-i} = w{end-i} + eta * d(end-i,1:c) .* v(end-i,1:c);
                end
            end
        end

        % Evolution Graph
        if(plotting == 0)
            if(m==1 && mod(cyc+1,indx)==0)
                figure(cyc+1);
                plot(x, data, 'bx', x, y, 'ro');
            end
            if(m==2 && mod(cyc+1,indx)==0)
                figure(cyc+1);
                plot3(x(:,1), x(:,2), data, 'bx', x(:,1), x(:,2), y, 'ro');
            end
        end

        e = data - y;
        total_error = sum(sum(abs(e)));
        if total_error <= max_error
            break;
        end
        cyc = cyc + 1;
    end
    
    % MLP last iteration
    for row = 1:n
        c = lays_nodes(1);
        v(1, 1:c) = x(row,:) * w{1} + b(1, 1:c);
        v(1, 1:c) = activation_function(v(1, 1:c), activation_type, 0);
        
        for i = 2:dw(1)
            p = lays_nodes(i-1);
            c = lays_nodes(i);
            v(i, 1:c) = v(i-1, 1:p) * w{i} + b(i, 1:c);
            if(i == dw(1))
                continue;
            end
            v(i, 1:c) = activation_function(v(i, 1:c), activation_type, 0);
        end
            y(row,:) = v(end, 1:c);
    end

    % Assigning Output Values
    weights = w;
    bias = b;
    y_hat = y;
    epochs = cyc;
    error = total_error;
end