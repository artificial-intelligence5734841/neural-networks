%% Clear Environment
clear
clc

%% Data Creation
x = 0.1: 1/22 :1;
data = ((1 + 0.6 * sin (2 * pi * x / 0.7)) + 0.3 * sin (2 * pi * x) / 2)';

%% RBFN
r = [0.14425 0.14425];
c = [0.19 0.873];

f = [F(x, c(1), r(1)); F(x, c(2), r(2))]';
eta = 0.15;
w = rand(2,1);
b = rand(1);

for i = 1:1000
    for n = 1:20
        y = f(n,1) * w(1,1) + f(n,2) * w(2,1) + b;
        e = data(n) - y;

        w = w + eta * e * f(n,:)';
        b = b + eta * e;
    end
end

for k = 1:20
    y(k) = f(k,1) * w(1,1) + f(k,2) * w(2,1) + b;
end

plot(x,data,'kx', x,y,'gx');


%% Function Definition
function y = F(x, c, r)
    y = exp( (-(x-c).^2)./(2*r^2) );
end
